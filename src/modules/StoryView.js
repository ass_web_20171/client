import React from 'react'
import Api from '../core/FakeApi'
import { Link } from 'react-router-dom'
export default class StoryView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { chapters: [], story: null };
    }
    componentDidMount() {
        var storyId = this.props.match.params.storyId;
        var story = Api.getStoryById(storyId);
        if (story) {
            this.setState({ story: story });
        }
        let chapters = Api.getChaptersByStory(this.props.match.params.storyId);
        console.log(chapters);
        if (chapters)
            this.setState({ chapters: chapters });
    }
    getChapLink(chap) {
        return <Link className="orange no-underline" to={"/read/" + chap.id + "/" + chap.title}>{chap.title}</Link>;
    }
    render() {
        let story = this.state.story;
        let elem = null;
        if (story !== null && story !== undefined) {
            elem = (
                <div className="fl ph2 db">
                    <div className="fl w-100 tc bb b--black">
                        <p className="f3 b ttu">{this.state.story.title}</p>
                    </div>
                    <div className="fl w-100 no-underline pa2">
                        <div className="fl w-30 pa2">
                            <img src={story.thumbnail} alt={story.title} />
                        </div>
                        <div className="fl w-60 pa2 tl">
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Tác giả
                            </div>
                                <div className="fl w-60 w-75-ns">
                                    <span>{": " + story.author}</span>
                                </div>
                            </div>
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Thể loại
                            </div>
                                <div className="fl w-60 w-75-ns">
                                    <span>:  {story.categories.map(
                                        categoryId => {
                                            let category = Api.getCategoryById(categoryId);
                                            return (<Link className="no-underline orange" to={"/search/category/" + categoryId}>{category.title}</Link>);
                                        }).reduce((prev, curent) => <span>{prev}, {curent}</span>)
                                    }
                                    </span>
                                </div>
                            </div>
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Năm
                            </div>
                                <div className="fl w-60 w-75-ns">
                                    <span>{": " + story.publishedDate}</span>
                                </div>
                            </div>
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Số chương
                            </div>
                                <div className="fl w-60 w-75-ns">
                                    <span>{": " + story.chapters.length + "/" + story.totalChap}</span>
                                </div>
                            </div>
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Lần đọc
                            </div>
                                <div className="fl w-60 w-75-ns">
                                    <span>{": " + story.viewCount}</span>
                                </div>
                            </div>
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Like
                            </div>
                                <div className="fl w-60 w-75-ns dib v-mid">
                                    :&nbsp;
                                <div className="fb-like" data-href={window.location} data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                </div>
                            </div>
                            <div className="fl w-100 mt2">
                                <div className="fl w-40 w-25-ns">
                                    Chương mới
                            </div>
                                <div className="fl w-60 w-75-ns dib v-mid">
                                    <span>: {this.getChapLink(this.state.chapters[this.state.chapters.length - 1])}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="fl w-100">
                        <p>
                            {story.description}
                        </p>
                    </div>
                    <div className="w-100 pa2 tc">
                        <table className="list-table w-100 center tl pa2" cellSpacing="0">
                            <thead>
                                <tr className="ttu">
                                    <td className="w-60">
                                        <strong>Chương</strong>
                                    </td>
                                    <td className="tr w-40">
                                        <strong>Cập nhật</strong>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.chapters.map((chapter, index) =>
                                        <tr key={index}>
                                            <td>
                                                <Link className="no-underline orange" to={"/read/" + chapter.id + "/" + chapter.title}>
                                                    {chapter.title}
                                                </Link>
                                            </td>
                                            <td className="tr">{chapter.updateTime}</td>
                                        </tr>
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                    <div className="fl w-100 center pa2">
                        <div className="w-100 center">
                            <div className="fb-comments center"
                                data-href={window.location}
                                data-width="100%"
                                data-numposts="5"></div>
                        </div>
                    </div>
                    <div className="cf"></div>
                </div>
            );
        }
        return elem;
    }
}