import React from 'react'
import { Link } from 'react-router-dom'
export class Panel extends React.Component {
    render() {
        return (
            <div className="panel bg-near-white ph2 pt0 w-100 dib relative mt2 ba b--black-20 shadow-3">
                <p className="f3 w-100 tl">
                    <span className="ttu orange bb b--orange">
                        {this.props.title}
                    </span>
                </p>
                <div className="fl w-100 mh3  db">
                    {this.props.children}
                </div>
                <div className="cf"></div>
            </div>

        );
    }
}
export class Item extends React.Component {
    render() {
        return (
            <div className="w-100 relative db tc">
                <Link to={this.props.url} className="db tc">
                    <p className="db w-100 hover-gold mb0 white tc absolute z-99 bg-black-60 bottom-0 overflow-hidden"
                        style={{
                            maxHeight: "41px",
                            fontSize: "12px",
                            lineHeight: "18px",
                            padding: "5px"
                        }} >
                        {this.props.title}
                    </p>
                    <img src={this.props.image}
                        className="di img-item"
                        alt={this.props.discription} />

                </Link>
                {this.props.label && <span className={"label " + this.props.label}></span>}
            </div>
        );
    }
}
export default Panel;