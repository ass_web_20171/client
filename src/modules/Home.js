import React from 'react'
import { Panel, Item } from './Panel'
import Api from '../core/FakeApi'
import Helper from '../core/Helper'
import Ranking from './Ranking'
import {Link} from 'react-router-dom'
class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hotStories: [],
            hotByCategories: [],
            newChapters: [],
        }
    }
    componentDidMount() {
        this.setState({ hotByCategories: Api.getHotByCategories() });
        this.setState({ hotStories: Api.getHotStories() });
        let newchapter = Api.getNewUpdateChapter();
        if (newchapter)
            this.setState({ newChapters: newchapter });
    }
    renderStoryInfo(story) {
        return (
            <div>
                <p className="ma0"><Link className="no-underline orange" to={"/story/" + story.id + "/" + story.title}>{story.title}</Link></p>
                <p className="ma0">Tác giả: {story.author} - Lượt xem: {story.viewCount}</p>
            </div>
        );

    }
    render() {
        return (
            <div className="w-100">
                <Panel title="Truyện hot">
                    {this.state.hotStories.map((story, index) =>
                        <div key={index} className="fl w-30 w-16-ns pa2 hover-gold tc">
                            <Item key={index} url={"/story/" + story.id + "/" + Helper.toUnsigned(story.title)}
                                image={story.thumbnail}
                                title={story.title}
                                label="label-hot"
                                discription={story.discription} />
                        </div>
                    )}
                </Panel>
                <div className="cf"></div>
                <div className="fl mt2 w-100 ba b--black-20 shadow-3 bg-near-white">
                    <div className="fl w-100 w-75-ns ph2">
                        <div className="w-100">
                            <p className="f3 ttu tl">Mới cập nhật:</p>
                            <table cellSpacing="0px" className="list-table">
                                <thead>
                                    <tr>
                                        <td className="tl b">Truyện</td>
                                        <td className="tr b">Chương</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.newChapters.map((chapter, index) =>
                                        <tr key={index}>
                                            <td className="tl">
                                                {this.renderStoryInfo(Api.getStoryById(chapter.storyId))}
                                            </td>
                                            <td className="tr">
                                                <p className="ma0">
                                                    <Link className="no-underline orange" to={"/read/" + chapter.id + "/" + chapter.title}>{chapter.title}</Link>
                                                </p>
                                                <p className="ma0">
                                                    Ngày cập nhật: {chapter.updateTime}
                                                </p>
                                            </td>
                                        </tr>

                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="fl w-100 w-25-ns">
                        <Ranking />
                        
                    </div>
                </div>
            </div>
        );
    }
}
export default Home;