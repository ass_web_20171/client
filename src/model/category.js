export default class Category{
    constructor(id,title,description,created_by){
        this.id=id;
        this.title=title;
        this.description=description;
        this.created_by=created_by;
    }
}