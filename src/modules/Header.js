import React from 'react'
import '../assets/tachyons.css'
import logo from '../assets/logo.svg'
class Header extends React.Component {
    constructor(props) {
        super(props);
        this.searchHandle = this.searchHandle.bind(this)
    }
    searchHandle() {
        let searchString = this.searchBox.value;
        window.location = '#/search/title/' + (searchString === "" ? " " : searchString);
    }
    render() {
        return (
            <div className="w-100 bg-header white tc App-header App-header-m v-mid">
                <div className="fl w-100 w-45-m  w-30-l"
                >
                    <div>
                        <div>
                            <div className="dib"><img src={logo} className="App-logo v-mid" alt="logo" /></div>
                            <div className="dib v-mid">
                                <div className="f4 di  App-title nosifer">
                                    <span className="db lh-copy">Master</span>
                                    <span className="db lh-copy">Comic</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="fl w-100 w-55-m w-70-l dib relative pv2 pa0-ns">
                    <div className="fl w-100 tc tr-l db">
                        <div className="fl w-70 w-80-ns pr0 pr3-ns">
                            <input className="input-reset outline-0 w-80 ba br4 b--black-20 pa2 mb2"
                                placeholder="Search"
                                ref={(searchBox) => this.searchBox = searchBox}
                                type="text" name="search" id="search" />

                        </div>
                        <div className="fl w-30 w-20-ns center">
                            <input className="mr0 mr2-ns ba b--black mb2 pv2 br4 bg-header-button pointer black w-90 w-80-ns"
                                type="button" value="Search" onClick={this.searchHandle} />
                        </div>
                    </div>
                </div>
                <div className="cf"></div>
            </div>

        );
    }
}
export default Header;