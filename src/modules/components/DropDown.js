import React from 'react'
export default class DropDown extends React.Component {
    render() {
        return (
            <div className="relative dropdown-hover db dib-ns">
                <a className={this.props.className + " white link topnav"}>{this.props.title}</a>
                <div className="dropdown-content bg-black-60 white w-100 tc absolute-ns  tl relative-ns mt3 pt3 z-1">
                    {this.props.children}
                </div>
            </div>
        )
    }
}