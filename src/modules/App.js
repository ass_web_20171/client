import React, { Component } from 'react'
import '../assets/tachyons.css'
import '../assets/App.css';
import { Switch, Route } from 'react-router-dom'
import Search from './Search'
import StoryView from './StoryView'
import Ranking from './Ranking'
class App extends Component {
  render() {
    return (
      <div className="fl db w-100 mt2 ba b--black-20 bg-near-white">
        <div className="w-100 ">
          <div className="fl w-100 w-75-ns">
            <Switch>
              <Route path="/search/:type/:searchString" component={Search} />
              <Route path="/story/:storyId/:storyTitle" component={StoryView} />
            </Switch>
          </div>
          <div className="fl w-100 w-25-ns">
            <Ranking />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
