import React from 'react'
import Api from '../core/FakeApi'
import { Link } from 'react-router-dom'
export default class Ranking extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: [] };
    }
    componentDidMount() {
        let data = Api.getTop10View();
        this.setState({ data: data });
    }
    render() {
        return (
            <div className={this.props.className}>
                <p className="ttu f3">Bảng xếp hạng</p>
                <table cellSpacing="0" className="w-90 center">
                    <tbody>
                        {this.state.data.map((story, index) =>
                            <tr key={index} >
                                <td className="bt b--black-20 pa0 ma0 tl w-20 pa1">
                                    <span className={"top-label top" + (index + 1)}>{index + 1}</span>
                                </td>
                                <td className="bt b--black-20 pa0 f6  ma0 tl pa1">
                                    <Link to={"/story/" + story.id + "/" + story.title}
                                    className="no-underline black hover-dark-blue"><p>{story.title}</p></Link>
                                    <p className="silver">
                                        Lượt đọc: {story.viewCount}
                                    </p>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }
}