import React from 'react'
import '../assets/tachyons.css'
import Api from '../core/FakeApi'
class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = { message: "" }
        this.onSubmit = this.onSubmit.bind(this);
    }
    onSubmit(e) {
        e.preventDefault();
        if (Api.login(this.username.value, this.password.value)) {
            if (this.props.onLoginSuccess) {
                this.props.onLoginSuccess(this.username.value);
            }
        }
        else {
            this.setState({ message: "Wrong username or password" });
        }
        //alert(JSON.stringify(this.state))
    }
    render() {
        return (
            <form className="measure center" onSubmit={this.onSubmit}>
                <fieldset id="sign_up" className="ba b--transparent ph0 mh0">
                    <legend className="f4 fw6 ph0 mh0">Sign In</legend>
                    {this.state.message !== "" ?
                        (<div className="mt3 red tc">
                            {this.state.message}
                        </div>) : null}
                    <div className="mt3">
                        <label className="db fw6 lh-copy f6" htmlFor="email-address">Email</label>
                        <input className="pa2 input-reset ba b--black bg-transparent hover-bg-black hover-white w-100"
                            type="text" name="email-address" id="email-address"
                            ref={(input) => this.username = input} />
                    </div>
                    <div className="mv3">
                        <label className="db fw6 lh-copy f6" htmlFor="password">Password</label>
                        <input className="b pa2 input-reset ba b--black bg-transparent hover-bg-black hover-white w-100"
                            type="password" name="password" id="password"
                            ref={(input) => this.password = input} />
                    </div>
                    <label className="pa0 ma0 lh-copy f6 pointer"><input type="checkbox" /> Remember me</label>
                </fieldset>
                <div className="">
                    <input className="b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib" type="submit" value="Sign in" />
                </div>
                <div className="lh-copy mt3">
                    <a href="#0" className="f6 link dim black db">Sign up</a>
                    <a href="#0" className="f6 link dim black db">Forgot your password?</a>
                </div>
            </form>
        );

    }
}
export default Login;