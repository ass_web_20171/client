import React from 'react'
import Api from '../core/FakeApi'
import {Link} from 'react-router-dom'
export default class ChapterView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { chapter: null, prevChap: null, nextChap: null };
    }
    componentDidMount() {
        let chapter = Api.getChaperById(this.props.match.params.chapId);
        if (chapter)
            this.setState({ chapter: chapter });
        let result = Api.getNextAndPrevChap(chapter);
        if (result.next) {
            this.setState({ nextChap: result.next });
        }
        if (result.prev) {
            this.setState({ prevChap: result.prev });
        }
    }
    render() {
        let chapter = this.state.chapter;
        let next = this.state.nextChap;
        let prev = this.state.prevChap;
        if (chapter !=null && chapter!=undefined)
            return (
                <div className="pa2 ba b--black-20 bg-near-white  w-100">
                    <div className="w-100 tc f4 ttu">
                        <span className="b">
                            {this.state.chapter.title}
                        </span>
                    </div>
                    <div className="w-100 f5 pa4 tl">
                        <p className="indent lh-copy" dangerouslySetInnerHTML={{ __html: this.state.chapter.content }} />
                    </div>
                    <div className="mt2 w-100 db tc center paging">
                        {prev && <Link className="dib ba bg-white no-underline b--black pa2" to={"/read/" + prev.id + "/" + prev.title}>Chap trước</Link>}
                        {next && <Link className="dib ba bg-white no-underline b--black pa2" to={"/read/" + next.id + "/" + next.title}>Chap sau</Link>}

                    </div>
                    <div className="cf"></div>
                    <div className="w-100 center pa2">
                        <div className="w-100 center">
                            <div className="fb-comments center"
                                data-href={window.location}
                                data-width="100%"
                                data-numposts="5"></div>
                        </div>
                    </div>
                </div>)
        else
            return null;

    }
}