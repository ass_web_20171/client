export default class Chapter {
    constructor(id, storyId, title, content) {
        this.id = id;
        this.title = title;
        this.storyId = storyId;
        this.content = content;
        let time = new Date(new Date() - Math.floor(Math.random() * 10000000000));
        this.updateTime = time.getDate().toString() + "-" + (time.getMonth() + 1).toString() + "-" + time.getFullYear().toString();
    }
}