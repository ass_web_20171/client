import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import App from './modules/App';
import ChapterView from './modules/ChapterView'
import Home from './modules/Home'
import NavBar from './modules/NavBar'
import Header from './modules/Header'
import Footer from './modules/Footer'
ReactDOM.render((
    <Router>
        <div className="App">
            <Header />
            <div className="cf"></div>
            <NavBar />
            <div className="cf"></div>
            <Switch>
                <Route exact path="/read/:chapId/:chapTitle" component={ChapterView} />
                <Route exact path="/home" component={Home} />
                <Redirect exact from="/" to="/home" />
                <Route path="/" component={App}/>
            </Switch>
            <div className="cf"></div>
            <Footer />
        </div>
    </Router>
), document.getElementById('root'));

registerServiceWorker();