import React from 'react'
import '../assets/tachyons.css'
class Modal extends React.Component {
    render() {
        return (
            <div className="db dib-ns black">
                <a
                    className={this.props.className + " pointer" || "b ph3 pv2 input-reset ba b--black bg-transparent grow pointer f6 dib"}
                    onClick={() => document.getElementById(this.props.id).style.display = 'block'}>
                    {this.props.name}
                </a>
                <div id={this.props.id} className="dn z-max fixed top-0 left-0 pt7-l modal animate">
                    <div className="w-90 ba b--black-10 mv1 modal-content relative">
                        <span className="close hover-red pointer"
                            onClick={() => document.getElementById(this.props.id).style.display = 'none'}>
                            &times;</span>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}
export default Modal;