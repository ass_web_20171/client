import React from 'react'
import Api from '../core/FakeApi'
import Helper from '../core/Helper'
import { Link } from 'react-router-dom'
class SearchResultItem extends React.Component {

    render() {
        let story = this.props.story;
        return (
            <div className="fl pa2 mt2 db">
                <div className="fl w-100 tc pa2 bb b--black-20">
                    <span className="db f4">{story.title}</span>
                </div>
                <div className="fl w-100 pa2 relative">
                    <div className="fl w-20 pa3">
                        <img src={story.thumbnail} alt={story.title} />
                    </div>
                    <div className="fl w-80 pa2 tc">
                        <div className="fl w-100">
                            <p className="f5 tl mb1 b">Nội dung:</p>
                            <p className="f6 tl ma1 indent">
                                {story.description}
                            </p>
                        </div>
                    </div>
                    <div className="absolute right-0 bottom-0">
                        <Link to={"/story/" + story.id + "/" + Helper.toUnsigned(story.title)}>Đọc truyện</Link>
                    </div>
                </div>

            </div>
        );
    }
}
class Search extends React.Component {
    constructor(props) {
        super(props);
        this.pageSize = 10;
        this.state = { stories: [], currentPage: 1, totalPage: 1 };

    }
    onButtonClick(page) {
        this.setState({ currentPage: page });
    }
    onPrevClick() {
        if (this.state.currentPage > 1) {
            this.setState({ currentPage: this.state.currentPage - 1 });
            
        }
    }
    onNextClick() {
        if (this.state.currentPage <this.state.totalPage) {
            this.setState({ currentPage: this.state.currentPage +1} );
            
        }

    }
    getStories() {
        let searchString = this.props.match.params.searchString;
        switch (this.props.match.params.type) {
            case "category":
                {
                    return Api.getStoriesByCategory(searchString);
                }
            case "title":
                {
                    console.log('title');
                    return Api.searchByName(searchString);
                }
            default:
                {
                    return [];
                }
        }
    }
    componentDidMount() {
        let stories = this.getStories();
        if (stories) {
            this.setState({ stories: stories });
            console.log("stearch" + stories.length);
            this.setState({ totalPage: Math.floor(stories.length / this.pageSize) + 1 });
        }
    }
    render() {
        return (
            <div className="db ph2">
                <div className="w-100 bb b--black-20">
                    <p className="f3">
                        Tìm được {this.state.stories.length} truyện
                    </p>
                </div>
                {this.state.stories.length > 0 ?
                    <div>
                        <table className="list-table w-100" cellSpacing="0px">
                            <thead>
                                <tr className="b ma3">
                                    <td></td>
                                    <td >Truyện</td>
                                    <td>Tác giả</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.stories.slice((this.state.currentPage - 1) * this.pageSize,
                                    this.state.currentPage * this.pageSize)
                                    .map((story, index) =>
                                        <tr key={index}>
                                            <td className="">
                                                <img src={story.thumbnail}
                                                    width="70px"
                                                    alt={story.title} />
                                            </td>
                                            <td className="">
                                                <Link className="no-underline orange" to={"/story/" + story.id + "/" + story.title}>{story.title}</Link>
                                                <p>
                                                    {story.categories.map(
                                                        categoryId => {
                                                            let category = Api.getCategoryById(categoryId);
                                                            return (<Link className="no-underline orange" to={"/search/category/" + categoryId}>{category.title}</Link>);
                                                        }).reduce((prev, curent) => <span>{prev}, {curent}</span>)
                                                    }
                                                </p>
                                            </td>
                                            <td className="">
                                                <span>{story.author}</span>
                                            </td>
                                            <td className="">
                                                <p>{story.chapters.length} chương</p>
                                                <p>{story.viewCount} lượt đọc</p>
                                            </td>
                                        </tr>
                                    )

                                }
                            </tbody>
                        </table>
                        {this.state.totalPage >= 2 && <div className="paging">
                            <input className="pa2 ba b--black-20 mv2 pointer bg-white black" value="Trước"
                                type="button"
                                onClick={this.onPrevClick.bind(this)} />
                            {Array.apply(null, { length: this.state.totalPage }).map(Number.call, Number).map((v, index) =>
                                <input key={index} className="pa2 ba b--black-20 mv2 pointer bg-white black" value={(index + 1).toString()}
                                    type="button" 
                                    onClick={this.onButtonClick.bind(this, index + 1)} />
                            )}
                            <input className="pa2 ba b--black-20 mv2 pointer bg-white black" value="Sau"
                                type="button"
                                onClick={this.onNextClick.bind(this)} />
                        </div>
                        }
                    </div>
                    : <div className="fl w-100 pa2 mt2 db">Not found</div>}
            </div>
        )
    }
}

export default Search;