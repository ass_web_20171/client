export default class Story {
    constructor(id, title, thumbnail, description, categories, author, publishedDate, chapters, totalChap = "?") {
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.description = description;
        this.categories = categories;
        this.author = author;
        this.publishedDate = publishedDate;
        this.chapters = chapters;
        this.viewCount = Math.floor(Math.random() * 100000);
        this.totalChap = totalChap;
    }
}
