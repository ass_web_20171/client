import React from 'react'
import $ from 'jquery'
export default class ColorPicker extends React.Component {
    constructor(props){
        super(props);
        this.state={colors:{bgHeader:''}};
        this.onBgHeaderChange=this.onBgHeaderChange.bind(this);
    }
    onBgHeaderChange(e){
        this.setState({colors:{bgHeader:e.target.value}});
        $('.bg-header').css({'background-color':this.state.colors.bgHeader});
    }
    render() {
        return (
            <div>
                <div className="db pa2">
                    <label htmlFor="gb-header">Background Header:</label>
                    <input type="color" name="bg-header" id="bg-header" onChange={this.onBgHeaderChange}/>
                </div>
            </div>
        );
    }
}