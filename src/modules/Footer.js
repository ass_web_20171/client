import React from 'react'
export default class Footer extends React.Component {
    render() {
        return (
            <div className="fl w-100 bg-near-white mt2 ba b--black-20 shadpw-3">

                <div className="center tc">
                    <p>
                        Copyright &copy; <strong>Master Comic</strong>. All right reserved
                    </p>
                </div>
                <div className="fl w-100">
                    <div className="fl w-100 w-50-ns">
                        <span className="f3 db">Master Comic</span>
                        <a className="no-underline orange" href="#">Liên hệ</a>
                    </div>
                    <div className="fl tc mt2 mt0-ns tr-ns w-100 w-50-ns social">
                        <img src="./images/social/facebook.svg" alt="Facebook" />
                        <img src="./images/social/twitter.svg" alt="Twitter" />
                        <img src="./images/social/youtube.svg" alt="Youtube" />
                        <img src="./images/social/instagram.svg" alt="Instagram" />
                    </div>

                </div>
            </div>
        );
    }
}