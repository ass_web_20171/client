import React from 'react'
import '../assets/tachyons.css'
import '../assets/App.css';
import { NavLink, Link } from 'react-router-dom'
import DropDown from './components/DropDown'
import Api from '../core/FakeApi'
import ColorPicker from './ColorPicker'
import Modal from './Modal'
import Login from './Login'
import $ from 'jquery'
class NavBar extends React.Component {
    constructor(props) {
        super(props);
        this.onLoginSuccess = this.onLoginSuccess.bind(this);
        this.state = { isLoggedIn: false, username: "", categories: [] }
        this.logout = this.logout.bind(this);
    }
    onChangeColor(e) {
        e.preventDefault();
        $('.bg-black').css({ 'background-color': 'red' });
    }

    onLoginSuccess(username) {
        this.setState({ isLoggedIn: true, username: username });
    }
    logout() {
        this.setState({ isLoggedIn: false, username: "" })
    }
    componentDidMount() {
        this.setState({ categories: Api.getCategories() });
    }
    render() {
        return (
            <nav className="ttu w-100 dk-bg-green-gray tc white absolute relative-ns top-0 z-9999">
                <input type="checkbox" id="burger" className="absolute top-2 left-1 dn" />
                <label htmlFor="burger" className="dn-ns pointer absolute top-1 left-1">
                    <span className="sr-only">Show menu</span>
                    <i className="fa fa-bars white dib f2"></i>
                </label>
                <div className="overflow-hidde menu db-ns dn w-100 tc pl0 pt3 mv0 f3 fw7 f5-ns shadow-3">
                    <NavLink to="/home" className="ph4 hover-gold no-hover-border mh2 db di-ns pt1 pb3 pv3-ns white link topnav">Home</NavLink>
                    <DropDown title="Thể loại" className="hover-gold  ph4 mh2 db di-ns pt1 pb3 pv3-ns">
                        {this.state.categories.map(
                            (category, index) =>
                                <Link key={index} to={"/search/category/" + category.id}
                                    className="link hover-gold topnav white db pv2">
                                    {category.title}
                                </Link>
                        )}
                    </DropDown>
                    {this.state.isLoggedIn ?
                        (
                            <DropDown title={this.state.username} className="ph4 mh2  db di-ns pt1 pb3 pv3-ns">
                                <a className="link hover-gold topnav white db pv2" onClick={this.logout}>Logout</a>
                            </DropDown>
                        ) :
                        (<Modal id="loginModal" name="Login" className="hover-gold  ph4 mh2 db di-ns pt1 pb3 pv3-ns white link topnav">
                            <Login onLoginSuccess={this.onLoginSuccess} />
                        </Modal>)
                    }
                </div>
            </nav>
        );
    }
}
export default NavBar